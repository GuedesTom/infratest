module "resource_group" {
  source = "./resource_group"
}

resource "azurerm_kubernetes_cluster" "projectGT" {
  name = "projetGT"
  location = var.location
  resource_group_name = var.resource_group_name
  dns_prefix = "projetGT"

  default_node_pool {
    name = "workers"
    vm_size = "Standard_D2s_v3"
    node_count = 3
  }
}